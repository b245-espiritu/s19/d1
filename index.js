console.log('Hello World')


function determinTyphoonIntensity(windSpeed){
    if(windSpeed<0){
        return "Invalid argument!"
    }
    else if(windSpeed <= 30){
        return "Tropical depression detected!"
    }

    else if(windSpeed <= 60){
        return "Not a typhoon yet!"
    }

    else if(windSpeed <=88){
        return "Tropical Storm detected!"
    }

    else if(windSpeed <=117){
        return "Severe Tropical Storm Detected!"
    }
    else if(windSpeed > 117) {
        return "Typhoon detected!"
    }
}

console.log(determinTyphoonIntensity(89));